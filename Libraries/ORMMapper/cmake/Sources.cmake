

set(SourceDir sources)
set(IncludeDir includes)

file(GLOB_RECURSE Sources "${PROJECT_SOURCE_DIR}/${SourceDir}/*.cpp")
file(GLOB_RECURSE Headers "${PROJECT_SOURCE_DIR}/${IncludeDir}/*.h")

set(SourceFiles
        ${Sources}
        ${Headers})

include_directories(${PROJECT_SOURCE_DIR}/${IncludeDir})