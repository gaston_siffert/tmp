//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_TABLE_H
#define MAPPER_TABLE_H

#include <string>
#include <vector>
#include <list>
#include <memory>

namespace ORMMapper
{

    template <typename T>
    using Table = std::list<std::shared_ptr<T>>;

}

#endif //MAPPER_TABLE_H
