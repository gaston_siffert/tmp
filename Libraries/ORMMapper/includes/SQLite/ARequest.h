//
//  ARequest.h
//  ORMMapper
//
//  Created by siffer_g on 19/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#ifndef __ORMMapper__ARequest__
#define __ORMMapper__ARequest__


#include "Database.h"
#include "IRequest.h"
#include "Query.h"
#include "IRequestDelegate.h"
#include "SQLite/Factory.h"

namespace ORMMapper
{
    namespace SQLite
    {
        ///
        /// Abstract class for the Requests
        /// Provide a system of events which call a method for every row in the request's results.
        ///

        template <typename Type>
        class ARequest : public IRequest
        {
        public:
            ARequest(IRequestDelegate *delegate) :
                    _query(this)
            {
                _delegate = delegate;
            }

            virtual ~ARequest() = default;

            ///
            /// Method called for every row readed
            /// It is updating the values into _columns and _values
            /// \param size => number of columns
            /// \param values => array of char* containing the row's values
            /// \param columns => array of char* containing the columns' name
            /// \return 0 in Success, anything else will abort the request
            ///
            int row(int size, char **values, char **columns)
            {
                _columns.clear();
                _values.clear();
                _columns.resize(size);
                _values.resize(size);

                for (int i = 0; i < size; ++i)
                {
                    _columns[i] = std::string(columns[i]);
                    _values[i] = !values[i] ? std::string() : std::string(values[i]);
                }
                return (rowRead());
            }

        protected:
            void onError(std::string const &error) const
            {
                _delegate->onError(error);
            }

            void requestFinished() const
            {
                _delegate->requestFinished();
            }

            ///
            /// Method used to read the request's result
            /// It will end by calling the rowReaded method
            /// \param request => SQLite request to run.
            /// \return the result of the request
            ///
            int slowRun(std::string const &request)
            {
                _items.clear();
                if (_query.slowRun(request) || !_values.size())
                    return -1;
                return 0;
            }

            ///
            /// Method used to execute a request whithout reading it's result
            /// \param request => SQLite request to run.
            /// \return the result of the request
            ///
            int fastRun(std::string const &request)
            {
                _items.clear();
                return (_query.fastRun(request));
            }

        private:
            int rowRead()
            {
                auto item = SQLite::Factory::instantiate<Type>(_columns, _values);
                if (!item)
                    return -1;
                _items.push_back(item);
                return 0;
            }

        protected:
            IRequestDelegate *_delegate;

            Table<Type> _items;
            std::vector<std::string>    _columns; ///< store the column's name
            std::vector<std::string>    _values; ///< store the row's values
            Query   _query; ///< Emulate and execute the query
        };

    }
}

#endif /* defined(__ORMMapper__ARequest__) */
