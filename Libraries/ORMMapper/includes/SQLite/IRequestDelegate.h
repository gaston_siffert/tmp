//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IREQUESTDELEGATE_H
#define MAPPER_IREQUESTDELEGATE_H

#include "Table.h"

namespace ORMMapper
{
    namespace SQLite
    {

        class IRequestDelegate
        {
        public:
            IRequestDelegate() = default;
            virtual ~IRequestDelegate() = default;

            virtual void onError(std::string const &error) const {};
            virtual void requestFinished() const {};
        };

    }
}

#endif //MAPPER_IREQUESTDELEGATE_H
