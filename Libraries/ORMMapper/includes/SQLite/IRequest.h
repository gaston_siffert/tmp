//
//  IRequest.h
//  ORMMapper
//
//  Created by siffer_g on 18/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#ifndef __ORMMapper__IRequest__
#define __ORMMapper__IRequest__

#include "Table.h"

namespace ORMMapper
{
    namespace SQLite
    {
        ///
        /// Base class for every request.
        ///
        class IRequest
        {
        public:
            virtual ~IRequest() = default;
            virtual int row(int size, char **values, char **columns) = 0;
            virtual void onError(std::string const &error) const = 0;
            virtual void requestFinished() const = 0;
        };

    }
}

#endif /* defined(__ORMMapper__IRequest__) */
