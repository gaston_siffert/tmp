//
//  Query.h
//  ORMMapper
//
//  Created by siffer_g on 18/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#ifndef __ORMMapper__Query__
#define __ORMMapper__Query__

# include <sqlite3.h>

# include "Database.h"
# include "IRequest.h"

namespace ORMMapper
{
    namespace SQLite
    {
        ///
        /// Class abstracting the query to the sqlite's library
        ///
        class Query
        {
        public:
            Query(IRequest *delegate);
            ~Query();

            int slowRun(std::string const &request);
            int fastRun(std::string const &request);

        private:
            void raiseError(char *data) const;

        private:
            IRequest    *_delegate;
        };

    }
}

#endif /* defined(__ORMMapper__Query__) */
