//
// Created by siffer_g on 21/08/15.
//

#ifndef ORMMAPPER_IMODEL_H
#define ORMMAPPER_IMODEL_H

#include "Table.h"

namespace ORMMapper
{

    namespace SQLite
    {

        class IModel
        {
        public:
            IModel() = default;
            virtual ~IModel() = default;

            virtual void initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values) = 0;
        };

    }
}

#endif //ORMMAPPER_IMODEL_H
