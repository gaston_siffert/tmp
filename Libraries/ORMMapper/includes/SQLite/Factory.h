//
// Created by siffer_g on 21/08/15.
//

#ifndef ORMMAPPER_FACTORY_H
#define ORMMAPPER_FACTORY_H

#include <memory>
# include <vector>
# include <string>
#include "Models/Tool.h"

namespace ORMMapper
{
    namespace SQLite
    {
        class Factory
        {
        public:

            template <typename Type>
            static std::shared_ptr<Type> instantiate(std::vector<std::string> const &columns,
                                                     std::vector<std::string> const &values)
            {
                std::shared_ptr<Type> target = std::make_shared<Type>();
                target->initialise(columns, values);
                return (target);
            }
        };

    }
}

#endif //ORMMAPPER_FACTORY_H
