//
//  Database.h
//  ORMMapper
//
//  Created by siffer_g on 18/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#ifndef __ORMMapper__Database__
#define __ORMMapper__Database__

# include <string>

# include <sqlite3.h>

namespace ORMMapper
{
    namespace SQLite
    {
        ///
        /// Abstraction of the sqlite3's library
        /// for handling the connections.
        ///
        class Database
        {
        private:
            Database();

        public:
            ~Database();

            bool isOpen() const;
            std::string getError() const;
            sqlite3 const *getDB() const;

            static Database const &getInstance();

        private:
            static std::string const _dbName; ///< Name of the database's file.
            bool _opened; ///< State of the connection.
            sqlite3 *_db; ///< Instance of the sqlite's database.
        };

    }
}

#endif /* defined(__ORMMapper__Database__) */
