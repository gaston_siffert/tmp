//
//  Decoration.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Decoration__
#define ____Decoration__

# include "AStaticUnit.h"

namespace ORMMapper
{
    namespace Models
    {

        class Decoration : AStaticUnit
        {
        public:
            Decoration() = default;
            ~Decoration() = default;
        };

    }
}

#endif /* defined(____Decoration__) */
