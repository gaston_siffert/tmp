//
//  AItem.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____AItem__
#define ____AItem__

#include "SQLite/IModel.h"
#include "Resource.h"
#include "Technology.h"
#include "Delegates/IResource.h"
#include "Delegates/ITechnology.h"

namespace ORMMapper
{
    namespace Models
    {

        class AItem : public SQLite::IModel,
                      public Delegates::IResource,
                      public Delegates::ITechnology
        {
        public:
            AItem() = default;
            virtual ~AItem() = default;

            virtual void initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values);

            unsigned int getID() const;
            std::shared_ptr<Resource> const &getResource() const;
            std::string const &getName() const;
            std::string const &getDescription() const;
            std::shared_ptr<ORMMapper::Models::Technology> const &getTechnology() const;

            /// ===== IResource =====

            void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Resource> const &resource);

            /// ===== ITechnology =====

            void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Technology> const &technology);

        protected:
            unsigned int _id;
            std::shared_ptr<Models::Resource> _resource;
            std::string _name;
            std::string _description;
            std::shared_ptr<Models::Technology> _technologies;
        };

    }
}

#endif /* defined(____AItem__) */
