//
//  FormationBuilding.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____FormationBuilding__
#define ____FormationBuilding__

# include <stack>

# include "AStaticUnit.h"
# include "AMovableUnit.h"
# include "Skill.h"

namespace ORMMapper
{
    namespace Models
    {

        class FormationBuilding : public AStaticUnit
        {
        public:
            FormationBuilding() = default;
            ~FormationBuilding() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getSpeed() const;
            unsigned int getCurrent() const;
            std::stack<std::shared_ptr<AMovableUnit>> const &getStudents() const;
            std::shared_ptr<Skill> const &getTeaching() const;

        private:
            unsigned int    _speed;
            unsigned int    _current;
            std::stack<std::shared_ptr<AMovableUnit>> _students;
            std::shared_ptr<Skill>  _teaching;
        };

    }
}

#endif /* defined(____FormationBuilding__) */
