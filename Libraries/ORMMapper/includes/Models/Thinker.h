//
//  Thinker.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Thinker__
#define ____Thinker__

# include "AMovableUnit.h"

namespace ORMMapper
{
    namespace Models
    {

        class Thinker : public AMovableUnit
        {
        public:
            Thinker() = default;
            ~Thinker() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getMilitaty() const;
            unsigned int getEconomy() const;

        private:
            unsigned int    _military;
            unsigned int    _economy;
        };

    }
}

#endif /* defined(____Thinker__) */
