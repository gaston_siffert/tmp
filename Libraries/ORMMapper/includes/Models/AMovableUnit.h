//
//  AMovableUnit.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____AMovableUnit__
#define ____AMovableUnit__

# include <list>

# include "ADestroyable.h"
# include "Skill.h"
# include "Task.h"

namespace ORMMapper
{
    namespace Models
    {

        class AMovableUnit : public ADestroyable
        {
        public:

            AMovableUnit() = default;
            virtual ~AMovableUnit() = default;

            virtual void initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values);

            unsigned int getAge() const;
            std::unique_ptr<Skill> const &getSkill() const;
            std::list<std::unique_ptr<Task>> const &getTasks() const;

        private:
            unsigned int    _age;
            std::unique_ptr<Skill>  _skill;
            std::list<std::unique_ptr<Task>> _tasks;

            // TODO Monture
        };

    }
}

#endif /* defined(____AMovableUnit__) */
