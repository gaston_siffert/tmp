//
//  Peasant.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Peasant__
#define ____Peasant__

# include "AMovableUnit.h"
# include "Tool.h"

namespace ORMMapper
{
    namespace Models
    {

        class Peasant : public AMovableUnit
        {
        public:
            Peasant() = default;
            ~Peasant() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            std::shared_ptr<Tool> const &getTool() const;

        private:
            std::shared_ptr<Tool>   _tool;
        };

    }
}

#endif /* defined(____Peasant__) */
