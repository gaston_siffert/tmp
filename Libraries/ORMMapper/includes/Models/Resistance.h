//
//  Resistance.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Resistance__
#define ____Resistance__

#include "SQLite/IModel.h"
#include "Weapon.h"
#include "Delegates/IWeaponCategory.h"

namespace ORMMapper
{
    namespace Models
    {

        class Resistance : public SQLite::IModel,
                           public Delegates::IWeaponCategory
        {
        public:
            Resistance() = default;
            ~Resistance() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            int getPercent() const;
            std::shared_ptr<ORMMapper::Models::WeaponCategory> const &getCategory() const;

            /// ===== IWeaponCategory =====

            void selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category);

        private:
            unsigned int _id;
            int _percent;
            std::shared_ptr<ORMMapper::Models::WeaponCategory>  _category;
        };

    }
}

#endif /* defined(____Resistance__) */
