//
//  AStaticUnit.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____AStaticUnit__
#define ____AStaticUnit__

# include <list>

# include "ADestroyable.h"
# include "Armor.h"
# include "AMovableUnit.h"

namespace ORMMapper
{
    namespace Models
    {

        class AStaticUnit : public ADestroyable
        {
        public:
            AStaticUnit() = default;
            virtual ~AStaticUnit() = default;

            virtual void initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values);

            std::shared_ptr<Armor> const &getArmor() const;
            std::list<std::shared_ptr<AMovableUnit>> const &getUnits() const;

        private:
            std::shared_ptr<Armor>  _armor;
            std::list<std::shared_ptr<AMovableUnit>>    _units;
        };

    }
}

#endif /* defined(____AStaticUnit__) */
