//
//  ProductionBuilding.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____ProductionBuilding__
#define ____ProductionBuilding__

# include "AStaticUnit.h"
# include "AItem.h"

namespace ORMMapper
{
    namespace Models
    {

        class ProductionBuilding : public AStaticUnit
        {
        public:
            ProductionBuilding() = default;
            ~ProductionBuilding() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getSpeed() const;
            unsigned int getCurrent() const;
            std::shared_ptr<AItem> const &getItem() const;

        private:
            unsigned int    _speed;
            unsigned int    _current;
            std::shared_ptr<AItem>  _item;
        };

    }
}

#endif /* defined(____ProductionBuilding__) */
