//
//  Armor.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Armor__
#define ____Armor__

#include "AItem.h"
#include "Resistance.h"
#include "Delegates/IResistance.h"

namespace ORMMapper
{
    namespace Models
    {

        class Armor : public AItem,
                      public Delegates::IResistance
        {
        public:
            Armor() = default;
            ~Armor() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getSolidity() const;
            Table<Resistance> const &getResistances() const;

            /// ===== IResistance =====

            void selectByArmorIDFinished(Table<ORMMapper::Models::Resistance> const &resistances);

        private:
            unsigned int    _solidity;
            Table<Resistance>   _resistances;
        };

    }
}

#endif /* defined(____Armor__) */
