//
//  ADestroyable.h
//  
//
//  Created by siffer_g on 28/07/2015.
//
//

#ifndef ____ADestroyable__
#define ____ADestroyable__


# include "SQLite/IModel.h"
# include "Models/Weapon.h"
# include "Models/AItem.h"

namespace ORMMapper
{
    namespace Models
    {
        class ADestroyable : public AItem
        {
        public:
            ADestroyable() = default;
            virtual ~ADestroyable() = default;

            virtual void initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values);

            unsigned int getLife() const;
            unsigned int getRange() const;

        private:
            unsigned int    _life;
            unsigned int    _range;
        };

    }
}

#endif /* defined(____ADestroyable__) */
