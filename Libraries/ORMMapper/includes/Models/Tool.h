//
//  Tool.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Tool__
#define ____Tool__

# include "AItem.h"
# include "SQLite/IModel.h"

namespace ORMMapper
{
    namespace Models
    {

        class Tool : public AItem
        {
        public:
            Tool() = default;
            ~Tool() = default;
        };

    }
}

#endif /* defined(____Tool__) */
