//
//  Consumption.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Consumption__
#define ____Consumption__

#include "SQLite/IModel.h"
#include "Resource.h"

namespace ORMMapper
{
    namespace Models
    {

        class Consumption : public SQLite::IModel
        {
        public:
            Consumption() = default;
            ~Consumption() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            std::shared_ptr<Resource> const &getResource() const;

        private:
            unsigned int _id;
            std::shared_ptr<Resource>   _resource;
        };

    }
}

#endif /* defined(____Consumption__) */
