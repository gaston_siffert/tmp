//
//  Skill.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Skill__
#define ____Skill__

#include "SQLite/IModel.h"

namespace ORMMapper
{
    namespace Models
    {

        class Skill : public SQLite::IModel
        {
        public:
            Skill() = default;
            ~Skill() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            unsigned int getDexterity() const;
            unsigned int getStrength() const;
            unsigned int getKnowledge() const;

        private:
            unsigned int    _id;
            unsigned int    _dexterity;
            unsigned int    _strength;
            unsigned int    _knowledge;
        };

    }
}

#endif /* defined(____Skill__) */
