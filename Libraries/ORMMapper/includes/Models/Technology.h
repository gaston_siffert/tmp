//
//  Technology.h
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#ifndef __Tmp__Technology__
#define __Tmp__Technology__

#include <string>
#include <list>

#include "SQLite/IModel.h"
#include "Models/TechnologyCategory.h"
#include "Delegates/ITechnologyCategory.h"
#include "Delegates/ITechnology.h"

namespace ORMMapper
{
    namespace Models
    {

        class Technology : public SQLite::IModel,
                           public Delegates::ITechnologyCategory,
                           public Delegates::ITechnology
        {
        public:
            Technology() = default;
            ~Technology() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            unsigned int getCost() const;
            std::string const &getName() const;
            std::string const &getDescription() const;
            std::list<std::shared_ptr<Technology>> const &getDependencies() const;
            std::shared_ptr<TechnologyCategory> const &getCategory() const;


            /// ===== ITechnologyCategory =====

            void selectByIDFinished(std::shared_ptr<TechnologyCategory> const &category);

            /// ===== ITechnology =====

            void selectDependenciesForIDFinished(
                    std::list<std::shared_ptr<Technology>> const &technologies);

        private:
            unsigned int    _id;
            unsigned int    _cost;
            std::string     _name;
            std::string     _description;
            std::list<std::shared_ptr<Technology>> _dependencies;
            std::shared_ptr<TechnologyCategory> _category;
        };

    }
}

#endif /* defined(__Tmp__Technology__) */
