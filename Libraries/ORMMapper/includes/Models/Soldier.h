//
//  Soldier.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Soldier__
#define ____Soldier__

# include "AMovableUnit.h"
# include "Weapon.h"
# include "Armor.h"

namespace ORMMapper
{
    namespace Models
    {

        class Soldier : public AMovableUnit
        {
        public:
            Soldier() = default;
            ~Soldier() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            std::shared_ptr<Weapon> const &getRight() const;
            std::shared_ptr<Weapon> const &getLeft() const;
            std::shared_ptr<Armor> const &getArmor() const;

        private:
            std::shared_ptr<Weapon> _right;
            std::shared_ptr<Weapon> _left;
            std::shared_ptr<Armor>  _armor;
        };

    }
}

#endif /* defined(____Soldier__) */
