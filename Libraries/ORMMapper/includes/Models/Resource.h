//
//  Resource.h
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#ifndef __Tmp__Resource__
#define __Tmp__Resource__

#include <SQLite/IModel.h>

namespace ORMMapper
{
    namespace Models
    {

        class Resource : public SQLite::IModel
        {
        public:
            Resource() = default;
            ~Resource() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            unsigned int getGold() const;
            unsigned int getWood() const;
            unsigned int getRock() const;
            unsigned int getFood() const;

        private:
            unsigned int _id;
            unsigned int _gold;
            unsigned int _wood;
            unsigned int _rock;
            unsigned int _food;
        };

    }
}

#endif /* defined(__Tmp__Resource__) */
