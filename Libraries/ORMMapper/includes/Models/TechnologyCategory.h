//
// Created by siffer_g on 24/09/15.
//

#ifndef MAPPER_TECHNOLOGYCATEGORY_H
#define MAPPER_TECHNOLOGYCATEGORY_H

# include "SQLite/IModel.h"

namespace ORMMapper
{
    namespace Models
    {
        ///
        /// A category of technology.
        ///
        class TechnologyCategory : public SQLite::IModel
        {
        public:
            TechnologyCategory() = default;
            ~TechnologyCategory() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            std::string const &getName() const;

        private:
            unsigned int _id; ///< ID in the database.
            std::string _name; ///< Name of the category.
        };

    }
}

#endif //MAPPER_TECHNOLOGYCATEGORY_H
