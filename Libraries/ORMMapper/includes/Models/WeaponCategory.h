//
// Created by siffer_g on 24/09/15.
//

#ifndef MAPPER_WEAPONCATEGORY_H
#define MAPPER_WEAPONCATEGORY_H

# include "SQLite/IModel.h"

namespace ORMMapper
{
    namespace Models
    {

        class WeaponCategory : public SQLite::IModel
        {
        public:
            WeaponCategory() = default;
            ~WeaponCategory() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            std::string const &getName() const;

        private:
            unsigned int _id;
            std::string _name;
        };

    }
}

#endif //MAPPER_WEAPONCATEGORY_H
