//
//  Weapon.h
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#ifndef __Tmp__Weapon__
#define __Tmp__Weapon__

#include <string>
#include <list>
#include <memory>

#include "Technology.h"
#include "WeaponCategory.h"
#include "AItem.h"
#include "SQLite/IModel.h"
#include "Delegates/IWeaponCategory.h"

namespace ORMMapper
{
    namespace Models
    {

        class Weapon : public AItem,
                       public Delegates::IWeaponCategory
        {
        public:
            Weapon() = default;
            ~Weapon() = default;
            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getDamage() const;
            unsigned int getRange() const;
            std::shared_ptr<WeaponCategory> const &getCategory() const;

            /// ===== IWeaponCategory =====

            void selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category);

        private:
            unsigned int    _damage;
            unsigned int    _range;
            std::shared_ptr<WeaponCategory> _category;
        };

    }
}

#endif /* defined(__Tmp__Weapon__) */
