//
//  Population.h
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#ifndef ____Population__
#define ____Population__

#include <list>

#include "Models/Peasant.h"
#include "Models/Soldier.h"
#include "Models/Thinker.h"
#include "SQLite/IModel.h"

namespace ORMMapper
{
    namespace Models
    {

        class Population : public SQLite::IModel
        {
        public:
            Population() = default;
            ~Population() = default;

            void initialise(std::vector<std::string> const &columns,
                            std::vector<std::string> const &values);

            unsigned int getID() const;
            unsigned int getMax() const;
            unsigned int getEmigration() const;
            unsigned int getImmigration() const;
            unsigned int getFecundity() const;
            unsigned int getMortality() const;
            int getHappiness() const;

            std::list<std::shared_ptr<Peasant>> const &getPeasants() const;
            std::list<std::shared_ptr<Soldier>> const &getSoldiers() const;
            std::list<std::shared_ptr<Thinker>> const &getThinkers() const;

        private:
            unsigned int    _id;
            unsigned int    _max;
            unsigned int    _immigration;
            unsigned int    _emigration;
            unsigned int    _fecundity;
            unsigned int    _mortality;
            int             _happiness;

            std::list<std::shared_ptr<Peasant>> _peasants;
            std::list<std::shared_ptr<Soldier>> _soldiers;
            std::list<std::shared_ptr<Thinker>> _thinkers;
        };

    }
}

#endif /* defined(____Population__) */
