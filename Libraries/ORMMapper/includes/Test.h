//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_TEST_H
#define MAPPER_TEST_H

#include "Delegates/ITechnology.h"
#include "Models/Technology.h"
#include "Delegates/IWeaponCategory.h"
#include "Delegates/IWeapon.h"
#include "Delegates/IArmor.h"

class Test : public ORMMapper::Delegates::ITechnology,
             public ORMMapper::Delegates::IWeaponCategory,
             public ORMMapper::Delegates::IWeapon,
             public ORMMapper::Delegates::IArmor
{
public:
    Test() = default;
    ~Test() = default;

    void selectAllFinished(ORMMapper::Table<ORMMapper::Models::Technology> const &technologies);
    void selectDependenciesForIDFinished(ORMMapper::Table<ORMMapper::Models::Technology> const &technologies);


    void selectAllFinished(ORMMapper::Table<ORMMapper::Models::WeaponCategory> const &categories);
    void selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &categories);

    void selectAllFinished(ORMMapper::Table<ORMMapper::Models::Weapon> const &weapons);

    void selectAllFinished(ORMMapper::Table<ORMMapper::Models::Armor> const &armors);
};


#endif //MAPPER_TEST_H
