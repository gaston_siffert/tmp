//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_RESOURCE_H
#define MAPPER_RESOURCE_H

#include "SQLite/ARequest.h"
#include "Models/Resource.h"
#include "Delegates/IResource.h"

namespace ORMMapper
{
    namespace Requests
    {

        class Resource : public SQLite::ARequest<ORMMapper::Models::Resource>
        {
        public:
            Resource(ORMMapper::Delegates::IResource *delegate);

            int selectByID(unsigned int id);
        };

    }
}

#endif //MAPPER_RESOURCE_H
