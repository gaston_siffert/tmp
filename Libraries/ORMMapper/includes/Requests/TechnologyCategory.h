//
// Created by siffer_g on 19/08/15.
//

#ifndef ORMMAPPER_TECHNOLOGYCATEGORY_H
#define ORMMAPPER_TECHNOLOGYCATEGORY_H

#include <list>

#include "SQLite/ARequest.h"
#include "Models/TechnologyCategory.h"
#include "Delegates/ITechnologyCategory.h"

namespace ORMMapper
{
    namespace Requests
    {

        class TechnologyCategory : public SQLite::ARequest<ORMMapper::Models::TechnologyCategory>
        {
        public:
            TechnologyCategory(ORMMapper::Delegates::ITechnologyCategory *delegate);
            ~TechnologyCategory() = default;

            int selectAll();
            int selectByID(unsigned int id);
        };

    }
}

#endif //ORMMAPPER_TECHNOLOGYCATEGORY_H
