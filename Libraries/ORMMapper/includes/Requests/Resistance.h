//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_RESISTANCE_H
#define MAPPER_RESISTANCE_H

#include "SQLite/ARequest.h"
#include "Delegates/IResistance.h"
#include "Models/Resistance.h"

namespace ORMMapper
{
    namespace Requests
    {

        class Resistance : public SQLite::ARequest<ORMMapper::Models::Resistance>
        {
        public:
            Resistance(ORMMapper::Delegates::IResistance *delegate);
            ~Resistance() = default;

            int selectByID(unsigned int id);
            int selectByArmorID(unsigned int id);
        };

    }
}

#endif //MAPPER_RESISTANCE_H
