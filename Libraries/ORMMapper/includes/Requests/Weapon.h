//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_WEAPON_H
#define MAPPER_WEAPON_H

#include "SQLite/ARequest.h"
#include "Delegates/IWeapon.h"
#include "Models/Weapon.h"

namespace ORMMapper
{
    namespace Requests
    {

        class Weapon : public SQLite::ARequest<ORMMapper::Models::Weapon>
        {
        public:
            Weapon(ORMMapper::Delegates::IWeapon *delegate);
            ~Weapon() = default;

            int selectAll();
        };

    }
}

#endif //MAPPER_WEAPON_H
