//
// Created by siffer_g on 21/08/15.
//

#ifndef ORMMAPPER_TOOL_H
#define ORMMAPPER_TOOL_H

# include <list>

# include "SQLite/ARequest.h"
# include "Models/Tool.h"

namespace ORMMapper
{
    namespace Requests
    {

        class Tool : public SQLite::ARequest
        {
        private:
            std::list<Models::Tool> _tools;

        protected:
            int rowReaded();

        public:
            int selectAll();
        };

    }
}

#endif //ORMMAPPER_TOOL_H
