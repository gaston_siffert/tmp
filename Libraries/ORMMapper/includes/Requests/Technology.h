//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_TECHNOLOGY_H
#define MAPPER_TECHNOLOGY_H

#include "SQLite/ARequest.h"

namespace ORMMapper
{
    namespace Models
    {
        class Technology;
    }

    namespace Delegates
    {
        class ITechnology;
    }

    namespace Requests
    {

        class Technology : public SQLite::ARequest<ORMMapper::Models::Technology>
        {
        public:
            Technology(ORMMapper::Delegates::ITechnology *delegate);
            ~Technology() = default;

            int selectAll();
            int selectByID(unsigned int id);
            int selectDependenciesForID(unsigned int id);
        };

    }
}

#endif //MAPPER_TECHNOLOGY_H
