//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_REQUESTS_WEAPONCATEGORY_H
#define MAPPER_REQUESTS_WEAPONCATEGORY_H

#include "SQLite/ARequest.h"
#include "Delegates/IWeaponCategory.h"

namespace ORMMapper
{
    namespace Requests
    {

        class WeaponCategory : public SQLite::ARequest<ORMMapper::Models::WeaponCategory>
        {
        public:
            WeaponCategory(Delegates::IWeaponCategory *delegate);
            ~WeaponCategory() = default;

            int selectAll();
            int selectByID(unsigned int id);
        };

    }
}

#endif //MAPPER_REQUESTS_WEAPONCATEGORY_H
