//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_ARMOR_H
#define MAPPER_ARMOR_H

#include "SQLite/ARequest.h"
#include "Delegates/IArmor.h"
#include "Models/Armor.h"

namespace ORMMapper
{
    namespace Requests
    {

        class Armor : public SQLite::ARequest<ORMMapper::Models::Armor>
        {
        public:
            Armor(ORMMapper::Delegates::IArmor *delegate);
            ~Armor() = default;

            int selectByID(unsigned int id);
            int selectAll();
        };

    }
}

#endif //MAPPER_ARMOR_H
