//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IWEAPONCATEGORY_H
#define MAPPER_IWEAPONCATEGORY_H

#include "SQLite/IRequestDelegate.h"
#include "Models/WeaponCategory.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class IWeaponCategory : public SQLite::IRequestDelegate
        {
        public:
            IWeaponCategory() = default;
            virtual ~IWeaponCategory() = default;

            virtual void selectAllFinished(Table<ORMMapper::Models::WeaponCategory> const &categories) {}
            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category) {}
        };

    }
}

#endif //MAPPER_IWEAPONCATEGORY_H
