//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_ITECHNOLOGY_H
#define MAPPER_ITECHNOLOGY_H

#include "SQLite/IRequestDelegate.h"

namespace ORMMapper
{
    namespace Models
    {
        class Technology;
    }

    namespace Delegates
    {

        class ITechnology : public SQLite::IRequestDelegate
        {
        public:
            virtual ~ITechnology() = default;

            virtual void selectAllFinished(Table<ORMMapper::Models::Technology> const &technologies) {}
            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Technology> const &technology) {}
            virtual void selectDependenciesForIDFinished(Table<ORMMapper::Models::Technology> const &technologies) {}
        };

    }
}

#endif //MAPPER_ITECHNOLOGY_H
