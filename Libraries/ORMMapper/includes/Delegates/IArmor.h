//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IARMOR_H
#define MAPPER_IARMOR_H

#include "SQLite/IRequestDelegate.h"
#include "Models/Armor.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class IArmor : public SQLite::IRequestDelegate
        {
        public:
            IArmor() = default;
            virtual ~IArmor() = default;

            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Armor> const &armor) {}
            virtual void selectAllFinished(ORMMapper::Table<ORMMapper::Models::Armor> const &armors) {}
        };

    }
}

#endif //MAPPER_IARMOR_H
