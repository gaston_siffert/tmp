//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IRESISTANCE_H
#define MAPPER_IRESISTANCE_H

#include "SQLite/IRequestDelegate.h"
#include "Models/Resistance.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class IResistance : public SQLite::IRequestDelegate
        {
        public:
            IResistance() = default;
            virtual ~IResistance() = default;

            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Resistance> const &resistance) {}
            virtual void selectByArmorIDFinished(Table<ORMMapper::Models::Resistance> const &resistances) {}
        };

    }
}

#endif //MAPPER_IRESISTANCE_H
