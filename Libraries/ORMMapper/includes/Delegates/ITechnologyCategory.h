//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IREQUEST_H
#define MAPPER_IREQUEST_H

#include <memory>
#include <list>

#include "SQLite/IRequestDelegate.h"
#include "Models/TechnologyCategory.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class ITechnologyCategory : public SQLite::IRequestDelegate
        {
        public:
            virtual ~ITechnologyCategory() = default;

            virtual void selectAllFinished(Table<ORMMapper::Models::TechnologyCategory> const &categories) {};
            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::TechnologyCategory> const &category) {};
        };

    }
}

#endif //MAPPER_IREQUEST_H
