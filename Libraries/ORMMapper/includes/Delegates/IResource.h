//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IRESOURCE_H
#define MAPPER_IRESOURCE_H

#include "SQLite/IRequestDelegate.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class IResource : public SQLite::IRequestDelegate
        {
        public:
            IResource() = default;
            virtual ~IResource() = default;

            virtual void selectByIDFinished(std::shared_ptr<ORMMapper::Models::Resource> const &resource) {}
        };

    }
}

#endif //MAPPER_IRESOURCE_H
