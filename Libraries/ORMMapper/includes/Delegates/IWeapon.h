//
// Created by siffer_g on 25/09/15.
//

#ifndef MAPPER_IWEAPON_H
#define MAPPER_IWEAPON_H

#include "SQLite/IRequestDelegate.h"
#include "Models/Weapon.h"

namespace ORMMapper
{
    namespace Delegates
    {

        class IWeapon : public SQLite::IRequestDelegate
        {
        public:
            IWeapon() = default;
            virtual ~IWeapon() = default;

            virtual void selectAllFinished(Table<ORMMapper::Models::Weapon> const &weapons) {}
        };

    }
}

#endif //MAPPER_IWEAPON_H
