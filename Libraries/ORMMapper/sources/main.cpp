#include "Requests/Technology.h"
#include "Requests/WeaponCategory.h"
#include "Requests/Weapon.h"
#include "Requests/Armor.h"

#include "Test.h"

int main()
{
    Test test;
    ORMMapper::Requests::Armor t(&test);

    t.selectAll();
//    t.selectByID(1);
//    t.selectDependenciesForID(4);
    return 0;
}