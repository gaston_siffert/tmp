//
//  Query.cpp
//  ORMMapper
//
//  Created by siffer_g on 18/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#include <iostream>

#include "SQLite/Query.h"

namespace ORMMapper
{
    namespace SQLite
    {

        static int callBack(void *callerInstance, int size,
                            char **values, char **columns)
        {
            IRequest *request = static_cast<IRequest *>(callerInstance);

            if (!request)
                return (-1);
            return (request->row(size, values, columns));
        }

        /// ===== Public =====

        Query::Query(IRequest *delegate)
        {
            _delegate = delegate;
        }

        Query::~Query() = default;

        ///
        /// Execute a request which will call the callBack
        /// it will be slower than a fast request but it
        /// will be possible to read the request's result.
        /// \param request => SQLite request
        /// \return result 0 in success
        ///
        int Query::slowRun(std::string const &request)
        {
            char *error = nullptr;

            if (sqlite3_exec(const_cast<sqlite3 *>(Database::getInstance().getDB()),
                             request.c_str(), &callBack, _delegate, &error) != 0)
            {
                raiseError(error);
                return -1;
            }
            _delegate->requestFinished();
            return 0;
        }

        ///
        /// Execute a request which won't call the callback
        /// \param request => SQLite request
        /// \return result 0 if success
        ///
        int Query::fastRun(std::string const &request)
        {
            char *error = nullptr;

            if (sqlite3_exec(const_cast<sqlite3 *>(Database::getInstance().getDB()),
                             request.c_str(), nullptr, nullptr, &error) != 0)
            {
                raiseError(error);
                return -1;
            }
            _delegate->requestFinished();
            return 0;
        }

        /// ===== Private =====

        void Query::raiseError(char *data) const
        {
            if (!data)
                return;
            std::string error(data);
            _delegate->onError(error);
            sqlite3_free(data);

        }

    }
}
