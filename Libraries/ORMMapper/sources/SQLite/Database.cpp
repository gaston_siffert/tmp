//
//  Database.cpp
//  ORMMapper
//
//  Created by siffer_g on 18/08/2015.
//  Copyright (c) 2015 siffer_g. All rights reserved.
//

#include "SQLite/Database.h"

namespace ORMMapper
{
    namespace SQLite
    {

        std::string const Database::_dbName = "tmp.db";

        /// ===== Public =====

        Database::~Database()
        {
            if (_opened)
            {
                _opened = false;
                sqlite3_close(_db);
            }
        }

        ///
        /// Get the state of the database.
        /// \return the state of the database.
        ///
        bool Database::isOpen() const
        {
            return (_opened);
        }

        ///
        /// Get the last error's message.
        /// \return the last error's message.
        ///
        std::string Database::getError() const
        {
            return (std::string(sqlite3_errmsg(_db)));
        }

        ///
        /// Get the database's instance.
        /// \return the database's instance.
        ///
        sqlite3 const *Database::getDB() const
        {
            return (_db);
        }

        ///
        /// Get the singleton
        /// \return the singleton of this class
        ///
        Database const &Database::getInstance()
        {
            static Database instance;

            return (instance);
        }

        /// ===== Private =====

        ///
        /// Instantiate and open the connection
        ///
        Database::Database()
        {
            _db = NULL;
            _opened = sqlite3_open(_dbName.c_str(), &_db) == SQLITE_OK;
        }

    }
}
