//
// Created by siffer_g on 25/09/15.
//

#include <iostream>

#include "Test.h"

void printResource(std::shared_ptr<ORMMapper::Models::Resource> const &resource)
{
    std::cout << "Resources: " << resource->getID() << ", ";
    std::cout << resource->getFood() << ", ";
    std::cout << resource->getRock() << ", ";
    std::cout << resource->getGold() << ", ";
    std::cout << resource->getWood() << std::endl;
}

void printTechnology(std::shared_ptr<ORMMapper::Models::Technology> const &technology)
{
    std::cout << "ID: " << technology->getID() << std::endl;
    std::cout << "Name: " << technology->getName() << std::endl;
    std::cout << "Description: " << technology->getDescription() << std::endl;
    std::cout << "CategoryName: " << technology->getCategory()->getName() << std::endl;
    std::cout << "Cost: " << technology->getCost() << std::endl;
    std::cout << "Dependencies: ";
    for (auto dependency : technology->getDependencies())
    {
        printTechnology(dependency);
    }
    std::cout << std::endl << std::endl;
}

void printResistance(std::shared_ptr<ORMMapper::Models::Resistance> const &resistance)
{
    std::cout << "ID: " << resistance->getID() << std::endl;
    std::cout << "Percent: " << resistance->getPercent() << std::endl;
    std::cout << "CategoryName: " << resistance->getCategory()->getName() << std::endl;
}

void printWeapon(std::shared_ptr<ORMMapper::Models::Weapon> const &weapon)
{
    std::cout << "ID: " << weapon->getID() << std::endl;
    std::cout << "Name: " << weapon->getName() << std::endl;
    std::cout << "Description: " << weapon->getDescription() << std::endl;
    std::cout << "Damage: " << weapon->getDamage() << std::endl;
    std::cout << "Range: " << weapon->getRange() << std::endl;
    std::cout << "CategoryName: " << weapon->getCategory()->getName() << std::endl;
    printResource(weapon->getResource());
    std::cout << "Technology: " << std::endl;
    printTechnology(weapon->getTechnology());
    std::cout << std::endl << std::endl;
}

void printArmor(std::shared_ptr<ORMMapper::Models::Armor> const &armor)
{
    std::cout << "ID: " << armor->getID() << std::endl;
    std::cout << "Name: " << armor->getName() << std::endl;
    std::cout << "Description: " << armor->getDescription() << std::endl;
    printResource(armor->getResource());
    for (auto item : armor->getResistances())
        printResistance(item);
    std::cout << "Technology: " << std::endl;
    printTechnology(armor->getTechnology());
    std::cout << std::endl << std::endl;
}

void Test::selectAllFinished(ORMMapper::Table<ORMMapper::Models::Technology> const &technologies)
{
    for (auto item : technologies)
        printTechnology(item);
}

void Test::selectDependenciesForIDFinished(ORMMapper::Table<ORMMapper::Models::Technology> const &technologies)
{
    for (auto item : technologies)
        printTechnology(item);
}

void Test::selectAllFinished(ORMMapper::Table<ORMMapper::Models::WeaponCategory> const &categories)
{
    for (auto item : categories)
        std::cout << item->getName() << std::endl;
}

void Test::selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category)
{
    std::cout << category->getName() << std::endl;
}

void Test::selectAllFinished(ORMMapper::Table<ORMMapper::Models::Weapon> const &weapons)
{
    for (auto item : weapons)
        printWeapon(item);
}

void Test::selectAllFinished(ORMMapper::Table<ORMMapper::Models::Armor> const &armors)
{
    for (auto item : armors)
        printArmor(item);
}