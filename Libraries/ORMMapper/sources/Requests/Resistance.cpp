//
// Created by siffer_g on 25/09/15.
//

#include <sstream>

#include "Requests/Resistance.h"

namespace ORMMapper
{
    namespace Requests
    {
        Resistance::Resistance(ORMMapper::Delegates::IResistance *delegate) :
                ARequest(delegate)
        {
        }

        int Resistance::selectByArmorID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT Resistances.* FROM Armors JOIN Resistances";
            ss << " ON Armors.ID = Resistances.ArmorID ";
            ss << " WHERE Armors.ID = " << id;
            if (slowRun(ss.str()) != 0)
                return -1;
            static_cast<ORMMapper::Delegates::IResistance *>(_delegate)->selectByArmorIDFinished(_items);
            return 0;
        }

        int Resistance::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT * FROM Resistance WHERE ID = " << id;
            if (slowRun(ss.str()) != 0)
                return -1;
            static_cast<ORMMapper::Delegates::IResistance *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }

    }
}