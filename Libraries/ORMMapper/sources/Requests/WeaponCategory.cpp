//
// Created by siffer_g on 25/09/15.
//

#include <sstream>

#include "Requests/WeaponCategory.h"
#include "SQLite/Factory.h"

namespace ORMMapper
{
    namespace Requests
    {

        WeaponCategory::WeaponCategory(Delegates::IWeaponCategory *delegate) :
            ARequest(delegate)
        {
        }

        int WeaponCategory::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT * FROM WeaponCategories WHERE ID = " << id;
            if (slowRun(ss.str()) != 0)
                return -1;
            static_cast<Delegates::IWeaponCategory *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }

        int WeaponCategory::selectAll()
        {
            if (slowRun("SELECT * FROM WeaponCategories") != 0)
                return -1;
            static_cast<Delegates::IWeaponCategory *>(_delegate)->selectAllFinished(_items);
            return 0;
        }
    }
}