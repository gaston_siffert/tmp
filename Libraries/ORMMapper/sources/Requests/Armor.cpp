//
// Created by siffer_g on 25/09/15.
//

#include <sstream>

#include "Requests/Armor.h"

namespace ORMMapper
{
    namespace Requests
    {

        Armor::Armor(ORMMapper::Delegates::IArmor *delegate) :
                ARequest(delegate)
        {
        }

        int Armor::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT * FROM Armors WHERE ID = " << id;
            if (slowRun(ss.str()) != 0)
                return -1;
            static_cast<ORMMapper::Delegates::IArmor *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }

        int Armor::selectAll()
        {
            if (slowRun("SELECT * FROM Armors") != 0)
                return -1;
            static_cast<ORMMapper::Delegates::IArmor *>(_delegate)->selectAllFinished(_items);
            return 0;
        }
    }
}