//
// Created by siffer_g on 25/09/15.
//

#include <sstream>

#include "Delegates/ITechnology.h"
#include "Requests/Technology.h"
#include "SQLite/Factory.h"

namespace ORMMapper
{
    namespace Requests
    {

        Technology::Technology(ORMMapper::Delegates::ITechnology *delegate) :
            ARequest(delegate)
        {
        }

        int Technology::selectAll()
        {
            if (slowRun("SELECT * FROM Technologies"))
                return -1;
            static_cast<Delegates::ITechnology *>(_delegate)->selectAllFinished(_items);
            return 0;
        }

        int Technology::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT * FROM Technologies WHERE ID = " << id;
            if (slowRun(ss.str()))
                return -1;
            static_cast<Delegates::ITechnology *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }

        int Technology::selectDependenciesForID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT Technologies.* FROM Technologies JOIN TechnologyDependencies";
            ss << " ON Technologies.ID = TechnologyDependencies.RequireID";
            ss << " WHERE TechnologyDependencies.CurrentID = " << id;
            if (slowRun(ss.str()))
                return -1;
            static_cast<Delegates::ITechnology *>(_delegate)->selectDependenciesForIDFinished(_items);
            return 0;
        }
    }
}