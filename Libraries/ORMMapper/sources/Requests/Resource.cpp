//
// Created by siffer_g on 25/09/15.
//

#include <sstream>

#include "Requests/Resource.h"

namespace ORMMapper
{
    namespace Requests
    {

        Resource::Resource(ORMMapper::Delegates::IResource *delegate) :
            ARequest(delegate)
        {
        }

        int Resource::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "SELECT * FROM Resources WHERE ID = " << id;
            if (slowRun(ss.str()) != 0)
                return -1;
            static_cast<ORMMapper::Delegates::IResource *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }


    }
}