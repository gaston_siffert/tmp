//
// Created by siffer_g on 25/09/15.
//

#include "Requests/Weapon.h"

namespace ORMMapper
{
    namespace Requests
    {

        Weapon::Weapon(ORMMapper::Delegates::IWeapon *delegate) :
                ARequest(delegate)
        {
        }

        int Weapon::selectAll()
        {
            if (slowRun("SELECT * FROM Weapons") != 0)
                return -1;
            static_cast<Delegates::IWeapon *>(_delegate)->selectAllFinished(_items);
            return 0;
        }
    }
}