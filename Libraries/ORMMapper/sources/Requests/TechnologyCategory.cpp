//
// Created by siffer_g on 19/08/15.
//

#include <sstream>

#include "SQLite/Factory.h"
#include "Requests/TechnologyCategory.h"

namespace ORMMapper
{
    namespace Requests
    {

        TechnologyCategory::TechnologyCategory(ORMMapper::Delegates::ITechnologyCategory *delegate) :
                ARequest(delegate)
        {
        }

        int TechnologyCategory::selectByID(unsigned int id)
        {
            std::stringstream ss;

            ss << "Select * FROM TechnologyCategories where ID=" << id;
            if (slowRun(ss.str()))
                return -1;
            static_cast<ORMMapper::Delegates::ITechnologyCategory *>(_delegate)->selectByIDFinished(_items.front());
            return 0;
        }

        int TechnologyCategory::selectAll()
        {
            if (slowRun("SELECT * from TechnologyCategories"))
                return -1;
            static_cast<ORMMapper::Delegates::ITechnologyCategory *>(_delegate)->selectAllFinished(_items);
            return 0;
        }

    }
}
