//
//  AStaticUnit.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/AStaticUnit.h"

namespace ORMMapper
{
    namespace Models
    {

        void AStaticUnit::initialise(std::vector<std::string> const &columns,
                                     std::vector<std::string> const &values)
        {
            ADestroyable::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Armor")
                {
                    //TODO Request by id
                }
            }
        }

        std::shared_ptr<Armor> const &AStaticUnit::getArmor() const
        {
            return _armor;
        }

        std::list<std::shared_ptr<AMovableUnit>> const &AStaticUnit::getUnits() const
        {
            return _units;
        }

    }
}
