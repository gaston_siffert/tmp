//
//  Resistance.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Resistance.h"
#include "Requests/WeaponCategory.h"

#include <iostream>

namespace ORMMapper
{
    namespace Models
    {

        void Resistance::initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Percent")
                    ss >> _percent;
                else if (columns[i] == "WeaponCategoryID")
                {
                    unsigned int id;

                    ss >> id;
                    Requests::WeaponCategory request(this);
                    request.selectByID(id);
                }
            }
        }

        unsigned int Resistance::getID() const
        {
            return _id;
        }

        int Resistance::getPercent() const
        {
            return _percent;
        }

        std::shared_ptr<ORMMapper::Models::WeaponCategory> const &Resistance::getCategory() const
        {
            return _category;
        }

        void Resistance::selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category)
        {
            _category = category;
        }
    }
}
