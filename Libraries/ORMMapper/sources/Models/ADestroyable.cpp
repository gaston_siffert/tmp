//
//  ADestroyable.cpp
//  
//
//  Created by siffer_g on 28/07/2015.
//
//

#include <sstream>

#include "Models/ADestroyable.h"

namespace ORMMapper
{
    namespace Models
    {

        void ADestroyable::initialise(std::vector<std::string> const &columns,
                                      std::vector<std::string> const &values)
        {
            AItem::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Life")
                    ss >> _life;
                else if (columns[i] == "Range")
                    ss >> _range;
            }
        }

        unsigned int ADestroyable::getLife() const
        {
            return _life;
        }

        unsigned int ADestroyable::getRange() const
        {
            return _range;
        }

    }
}
