//
//  Armor.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Armor.h"
#include "Requests/Resistance.h"

namespace ORMMapper
{
    namespace Models
    {

        void Armor::initialise(std::vector<std::string> const &columns,
                               std::vector<std::string> const &values)
        {
            AItem::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Solidity")
                    ss >> _solidity;
            }
            Requests::Resistance request(this);
            request.selectByArmorID(_id);
        }

        unsigned int Armor::getSolidity() const
        {
            return _solidity;
        }

        std::list<std::shared_ptr<Resistance>> const &Armor::getResistances() const
        {
            return _resistances;
        }

        void Armor::selectByArmorIDFinished(Table<ORMMapper::Models::Resistance> const &resistances)
        {
            _resistances = resistances;
        }
    }
}
