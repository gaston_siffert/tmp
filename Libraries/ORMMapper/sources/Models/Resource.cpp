//
//  Resource.cpp
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#include <sstream>

#include "Models/Resource.h"

namespace ORMMapper
{
    namespace Models
    {

        void Resource::initialise(std::vector<std::string> const &columns,
                                  std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Gold")
                    ss >> _gold;
                else if (columns[i] == "Rock")
                    ss >> _rock;
                else if (columns[i] == "Food")
                    ss >> _food;
                else if (columns[i] == "Wood")
                    ss >> _wood;
            }
        }

        unsigned int Resource::getID() const
        {
            return _id;
        }

        unsigned int Resource::getGold() const
        {
            return _gold;
        }

        unsigned int Resource::getWood() const
        {
            return _wood;
        }

        unsigned int Resource::getRock() const
        {
            return _rock;
        }

        unsigned int Resource::getFood() const
        {
            return _food;
        }

    }
}
