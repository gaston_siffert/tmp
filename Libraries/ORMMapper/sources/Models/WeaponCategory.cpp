//
// Created by siffer_g on 24/09/15.
//

#include <sstream>

#include "Models/WeaponCategory.h"

namespace ORMMapper
{
    namespace Models
    {
        /// ===== Public =====

        void WeaponCategory::initialise(std::vector<std::string> const &columns,
                                        std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Name")
                    _name = ss.str();
            }
        }

        unsigned int WeaponCategory::getID() const
        {
            return _id;
        }

        std::string const &WeaponCategory::getName() const
        {
            return _name;
        }

    }
}
