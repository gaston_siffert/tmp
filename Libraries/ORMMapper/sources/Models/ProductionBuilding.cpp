//
//  ProductionBuilding.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/ProductionBuilding.h"

namespace ORMMapper
{
    namespace Models
    {

        void ProductionBuilding::initialise(std::vector<std::string> const &columns,
                                            std::vector<std::string> const &values)
        {
            AStaticUnit::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Item")
                {
                    //TODO request by id
                }
                else if (columns[i] == "Current")
                    ss >> _current;
                else if (columns[i] == "Speed")
                    ss >> _speed;
            }
        }

        std::shared_ptr<AItem> const &ProductionBuilding::getItem() const
        {
            return _item;
        }

        unsigned int ProductionBuilding::getCurrent() const
        {
            return _current;
        }

        unsigned int ProductionBuilding::getSpeed() const
        {
            return _speed;
        }

    }
}
