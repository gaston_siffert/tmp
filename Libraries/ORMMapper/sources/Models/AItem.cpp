//
//  AItem.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/AItem.h"
#include "Requests/Resource.h"
#include "Requests/Technology.h"

namespace ORMMapper
{
    namespace Models
    {

        void AItem::initialise(std::vector<std::string> const &columns,
                               std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "ResourceID")
                {
                    unsigned int id;

                    ss >> id;
                    Requests::Resource request(this);
                    request.selectByID(id);
                }
                else if (columns[i] == "Name")
                    _name = ss.str();
                else if (columns[i] == "Description")
                    _description = ss.str();
                else if (columns[i] == "TechnologyID")
                {
                    unsigned int id;

                    ss >> id;
                    Requests::Technology request(this);
                    request.selectByID(id);
                }
            }
            //TODO gather the technologies
        }

        unsigned int AItem::getID() const
        {
            return _id;
        }

        std::shared_ptr<Resource> const &AItem::getResource() const
        {
            return _resource;
        }

        std::string const &AItem::getName() const
        {
            return _name;
        }

        std::string const &AItem::getDescription() const
        {
            return _description;
        }

        std::shared_ptr<ORMMapper::Models::Technology> const &AItem::getTechnology() const
        {
            return _technologies;
        }

        /// ===== IResource =====

        void AItem::selectByIDFinished(std::shared_ptr<ORMMapper::Models::Resource> const &resource)
        {
            _resource = resource;
        }

        /// ===== ITechnology =====

        void AItem::selectByIDFinished(std::shared_ptr<ORMMapper::Models::Technology> const &technology)
        {
            _technologies = technology;
        }
    }
}
