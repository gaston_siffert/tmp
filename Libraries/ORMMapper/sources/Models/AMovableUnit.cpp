
//
//  AMovableUnit.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/AMovableUnit.h"

namespace ORMMapper
{
    namespace Models
    {

        void AMovableUnit::initialise(std::vector<std::string> const &columns,
                                      std::vector<std::string> const &values)
        {
            AItem::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Age")
                    ss >> _age;
                else if (columns[i] == "SkillID")
                {
                    //TODO Request by id
                }
            }
        }

        unsigned int AMovableUnit::getAge() const
        {
            return _age;
        }

        std::unique_ptr<Skill> const &AMovableUnit::getSkill() const
        {
            return _skill;
        }

        std::list<std::unique_ptr<Task>> const &AMovableUnit::getTasks() const
        {
            return _tasks;
        }

    }
}
