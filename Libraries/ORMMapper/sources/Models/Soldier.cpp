//
//  Soldier.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Soldier.h"

namespace ORMMapper
{
    namespace Models
    {

        void Soldier::initialise(std::vector<std::string> const &columns,
                                 std::vector<std::string> const &values)
        {
            AMovableUnit::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "RightID")
                {
                    //TODO request by id
                }
                else if (columns[i] == "LeftId")
                {
                    //TODO request by id
                }
                else if (columns[i] == "ArmorID")
                {
                    //TODO request by id
                }
            }
        }

        std::shared_ptr<Weapon> const &Soldier::getRight() const
        {
            return _right;
        }

        std::shared_ptr<Weapon> const &Soldier::getLeft() const
        {
            return _left;
        }

        std::shared_ptr<Armor> const &Soldier::getArmor() const
        {
            return _armor;
        }

    }
}
