//
// Created by siffer_g on 24/09/15.
//

#include <sstream>

#include "Models/TechnologyCategory.h"

namespace ORMMapper
{
    namespace Models
    {
        /// ===== Public =====

        ///
        /// Fulfill the content of the model with the request's values
        /// \param columns => name of the columns
        /// \param values => row's values
        ///
        void TechnologyCategory::initialise(std::vector<std::string> const &columns,
                                            std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Name")
                    _name = ss.str();
            }
        }

        unsigned int TechnologyCategory::getID() const
        {
            return _id;
        }

        std::string const &TechnologyCategory::getName() const
        {
            return _name;
        }

    }
}

