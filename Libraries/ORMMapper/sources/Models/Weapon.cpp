//
//  Weapon.cpp
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#include <sstream>
#include <iostream>

#include "Models/Weapon.h"
#include "Requests/WeaponCategory.h"

namespace ORMMapper
{
    namespace Models
    {

        void Weapon::initialise(std::vector<std::string> const &columns,
                                std::vector<std::string> const &values)
        {
            AItem::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Damage")
                    ss >> _damage;
                else if (columns[i] == "Range")
                    ss >> _range;
                else if (columns[i] == "CategoryID")
                {
                    unsigned int id;

                    ss >> id;
                    Requests::WeaponCategory request(this);
                    request.selectByID(id);
                }
            }
        }

        unsigned int Weapon::getDamage() const
        {
            return _damage;
        }

        unsigned int Weapon::getRange() const
        {
            return _range;
        }

        std::shared_ptr<WeaponCategory> const &Weapon::getCategory() const
        {
            return _category;
        }

        /// ===== IWeaponCategory =====

        void Weapon::selectByIDFinished(std::shared_ptr<ORMMapper::Models::WeaponCategory> const &category)
        {
            _category = category;
        }
    }
}
