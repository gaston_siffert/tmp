//
//  Consumption.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Consumption.h"

namespace ORMMapper
{
    namespace Models
    {

        void Consumption::initialise(std::vector<std::string> const &columns,
                                     std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Resource")
                {
                    //TODO request by id
                }
            }
        }

        unsigned int Consumption::getID() const
        {
            return _id;
        }

        std::shared_ptr<Resource> const &Consumption::getResource() const
        {
            return _resource;
        }

    }
}
