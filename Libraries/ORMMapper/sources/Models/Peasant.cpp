//
//  Peasant.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include "Models/Peasant.h"

namespace ORMMapper
{
    namespace Models
    {

        void Peasant::initialise(std::vector<std::string> const &columns,
                                 std::vector<std::string> const &values)
        {
            AMovableUnit::initialise(columns, values);
        }

        std::shared_ptr<Tool> const &Peasant::getTool() const
        {
            return _tool;
        }

    }
}
