//
//  Skill.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Skill.h"

namespace ORMMapper
{
    namespace Models
    {

        void Skill::initialise(std::vector<std::string> const &columns,
                               std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Dexterity")
                    ss >> _dexterity;
                else if (columns[i] == "Strenght")
                    ss >> _strength;
                else if (columns[i] == "Knowledge")
                    ss >> _knowledge;
            }
        }

        unsigned int Skill::getID() const
        {
            return _id;
        }

        unsigned int Skill::getDexterity() const
        {
            return _dexterity;
        }

        unsigned int Skill::getStrength() const
        {
            return _strength;
        }

        unsigned int Skill::getKnowledge() const
        {
            return _knowledge;
        }

    }
}
