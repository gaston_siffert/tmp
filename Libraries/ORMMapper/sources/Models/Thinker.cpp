//
//  Thinker.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Thinker.h"

namespace ORMMapper
{
    namespace Models
    {

        void Thinker::initialise(std::vector<std::string> const &columns,
                                 std::vector<std::string> const &values)
        {
            AMovableUnit::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Military")
                    ss >> _military;
                else if (columns[i] == "Economy")
                    ss >> _economy;
            }
        }

        unsigned int Thinker::getMilitaty() const
        {
            return _military;
        }

        unsigned int Thinker::getEconomy() const
        {
            return _economy;
        }

    }
}
