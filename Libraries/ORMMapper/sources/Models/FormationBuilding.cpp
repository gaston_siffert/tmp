//
//  FormationBuilding.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/FormationBuilding.h"

namespace ORMMapper
{
    namespace Models
    {

        void FormationBuilding::initialise(std::vector<std::string> const &columns,
                                           std::vector<std::string> const &values)
        {
            AStaticUnit::initialise(columns, values);
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "Speed")
                    ss >> _speed;
                else if (columns[i] == "Current")
                    ss >> _current;
                else if (columns[i] == "SkillID")
                {
                    //TODO request by id
                }
            }
            //TODO get students
        }

        unsigned int FormationBuilding::getSpeed() const
        {
            return _speed;
        }

        unsigned int FormationBuilding::getCurrent() const
        {
            return _current;
        }

        std::stack<std::shared_ptr<AMovableUnit>> const &FormationBuilding::getStudents() const
        {
            return _students;
        }

        std::shared_ptr<Skill> const &FormationBuilding::getTeaching() const
        {
            return _teaching;
        }

    }
}
