//
//  Technology.cpp
//  Tmp
//
//  Created by siffer_g on 28/07/2015.
//
//

#include <sstream>

#include "Models/Technology.h"
#include "Requests/Technology.h"
#include "Requests/TechnologyCategory.h"

namespace ORMMapper
{
    namespace Models
    {

        /// ===== Public =====

        void Technology::initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Name")
                    _name = ss.str();
                else if (columns[i] == "Cost")
                    ss >> _cost;
                else if (columns[i] == "Description")
                    _description = ss.str();
                else if (columns[i] == "CategoryID")
                {
                    Requests::TechnologyCategory tc(this);
                    unsigned int id;

                    ss >> id;
                    tc.selectByID(id);
                }
            }
            Requests::Technology t(this);
            t.selectDependenciesForID(_id);
        }

        unsigned int Technology::getID() const
        {
            return _id;
        }

        unsigned int Technology::getCost() const
        {
            return _cost;
        }

        std::string const &Technology::getName() const
        {
            return _name;
        }

        std::string const &Technology::getDescription() const
        {
            return _description;
        }

        std::list<std::shared_ptr<Technology>> const &Technology::getDependencies() const
        {
            return _dependencies;
        }

        std::shared_ptr<TechnologyCategory> const &Technology::getCategory() const
        {
            return _category;
        }

        /// ===== ITechnologyCategory =====

        void Technology::selectByIDFinished(std::shared_ptr<ORMMapper::Models::TechnologyCategory> const &category)
        {
            _category = category;
        }

        /// ===== ITechnology =====

        void Technology::selectDependenciesForIDFinished(
                std::list<std::shared_ptr<ORMMapper::Models::Technology>> const &technologies)
        {
            _dependencies = technologies;
        }
    }
}
