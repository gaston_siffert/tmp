//
//  Population.cpp
//  
//
//  Created by siffer_g on 13/08/2015.
//
//

#include <sstream>

#include "Models/Population.h"

namespace ORMMapper
{
    namespace Models
    {

        void Population::initialise(std::vector<std::string> const &columns,
                                    std::vector<std::string> const &values)
        {
            for (int i = 0; i < columns.size(); ++i)
            {
                std::stringstream ss;

                ss << values[i];
                if (columns[i] == "ID")
                    ss >> _id;
                else if (columns[i] == "Max")
                    ss >> _max;
                else if (columns[i] == "Emigration")
                    ss >> _emigration;
                else if (columns[i] == "Immigration")
                    ss >> _immigration;
                else if (columns[i] == "Fecondity")
                    ss >> _fecundity;
                else if (columns[i] == "Mortality")
                    ss >> _mortality;
            }
            //TODO get the peasants
            //TODO get the soldiers
            //TODO get the thinkers
        }

        unsigned int Population::getID() const
        {
            return _id;
        }

        unsigned int Population::getMax() const
        {
            return _max;
        }

        unsigned int Population::getEmigration() const
        {
            return _emigration;
        }

        unsigned int Population::getImmigration() const
        {
            return _immigration;
        }

        unsigned int Population::getFecundity() const
        {
            return _fecundity;
        }

        unsigned int Population::getMortality() const
        {
            return _mortality;
        }

        int Population::getHappiness() const
        {
            return _happiness;
        }

        std::list<std::shared_ptr<Peasant>> const &Population::getPeasants() const
        {
            return _peasants;
        }

        std::list<std::shared_ptr<Soldier>> const &Population::getSoldiers() const
        {
            return _soldiers;
        }

        std::list<std::shared_ptr<Thinker>> const &Population::getThinkers() const
        {
            return _thinkers;
        }

    }
}
