//
//  Campaign.h
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#ifndef ____Campaign__
#define ____Campaign__

# include <stdio.h>
# include <cocos2d.h>

namespace Scenes
{
    
    class Campaign : public cocos2d::Layer
    {
    public:
        CREATE_FUNC(Campaign)
        static cocos2d::Scene   *createScene();
        virtual bool            init();
    };
    
}

#endif /* defined(____Campaign__) */
