//
//  StandardGame.h
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#ifndef ____StandardGame__
#define ____StandardGame__

# include <stdio.h>
# include <cocos2d.h>

namespace Scenes
{
    
    class StandardGame : public cocos2d::Layer
    {
    public:
        CREATE_FUNC(StandardGame)
        static cocos2d::Scene   *createScene();
        virtual bool            init();
    };
    
}

#endif /* defined(____StandardGame__) */
