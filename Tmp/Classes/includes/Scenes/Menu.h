//
//  Menu.h
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#ifndef ____Menu__
#define ____Menu__

# include <stdio.h>
# include <cocos2d.h>

namespace Scenes
{
    
    class Menu : public cocos2d::Layer
    {
    public:
        CREATE_FUNC(Menu)
        static cocos2d::Scene   *createScene();
        virtual bool            init();
        
    private:
        void    addBackground();
        void    addMenu();
        
        // Menu functions
        
        void    campaign(cocos2d::Ref *sender);
        void    standard(cocos2d::Ref *sender);
        void    multiplayer(cocos2d::Ref *sender);
        void    load(cocos2d::Ref *sender);
        void    exit(cocos2d::Ref *sender);
    };
    
}

#endif /* defined(____Menu__) */
