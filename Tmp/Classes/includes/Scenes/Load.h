//
//  Load.h
//  Tmp
//
//  Created by siffer_g on 29/07/2015.
//
//

#ifndef __Tmp__Load__
#define __Tmp__Load__

# include <stdio.h>
# include <cocos2d.h>

namespace Scenes
{
    
    class Load : public cocos2d::Layer
    {
    public:
        CREATE_FUNC(Load)
        static cocos2d::Scene   *createScene();
        virtual bool            init();
    };

}
#endif /* defined(__Tmp__Load__) */
