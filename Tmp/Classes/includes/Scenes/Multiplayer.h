//
//  Multiplayer.h
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#ifndef ____Multiplayer__
#define ____Multiplayer__

# include <stdio.h>
# include <cocos2d.h>

namespace Scenes
{
    
    class Multiplayer : public cocos2d::Layer
    {
    public:
        CREATE_FUNC(Multiplayer)
        static cocos2d::Scene   *createScene();
        virtual bool            init();
    };
    
}

#endif /* defined(____Multiplayer__) */
