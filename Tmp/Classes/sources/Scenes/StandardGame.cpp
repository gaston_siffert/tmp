//
//  StandardGame.cpp
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#include "Scenes/StandardGame.h"

namespace Scenes
{
    
    cocos2d::Scene  *StandardGame::createScene()
    {
        auto scene  = cocos2d::Scene::create();
        auto layer  = StandardGame::create();
        
        scene->addChild(layer);
        return (scene);
    }
    
    bool    StandardGame::init()
    {
        if (!cocos2d::Layer::init())
            return (false);
        return (true);
    }
}