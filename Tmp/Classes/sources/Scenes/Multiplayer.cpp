//
//  Multiplayer.cpp
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#include "Scenes/Multiplayer.h"

namespace Scenes
{
    
    cocos2d::Scene  *Multiplayer::createScene()
    {
        auto scene  = cocos2d::Scene::create();
        auto layer  = Multiplayer::create();
        
        scene->addChild(layer);
        return (scene);
    }
    
    bool    Multiplayer::init()
    {
        if (!cocos2d::Layer::init())
            return (false);
        return (true);
    }
}