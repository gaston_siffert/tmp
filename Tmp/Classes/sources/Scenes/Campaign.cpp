//
//  Campaign.cpp
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#include "Scenes/Campaign.h"

namespace Scenes
{
    
    cocos2d::Scene  *Campaign::createScene()
    {
        auto scene  = cocos2d::Scene::create();
        auto layer  = Campaign::create();
        
        scene->addChild(layer);
        return (scene);
    }
    
    bool    Campaign::init()
    {
        if (!cocos2d::Layer::init())
            return (false);
        auto map = cocos2d::TMXTiledMap::create("./res/Maps/first_map.tmx");
        addChild(map);
        return (true);
    }
}