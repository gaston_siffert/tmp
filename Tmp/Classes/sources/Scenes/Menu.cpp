//
//  Menu.cpp
//  
//
//  Created by siffer_g on 29/07/2015.
//
//

#include "Scenes/Menu.h"
#include "Scenes/Campaign.h"
#include "Scenes/Load.h"
#include "Scenes/Multiplayer.h"
#include "Scenes/StandardGame.h"

namespace Scenes
{
    
    cocos2d::Scene   *Menu::createScene()
    {
        auto scene  = cocos2d::Scene::create();
        auto layer  = Menu::create();
        
        scene->addChild(layer);
        return (scene);
    }
    
    bool Menu::init()
    {
        if (!cocos2d::Layer::init())
            return (false);
        addBackground();
        addMenu();
        return (true);
    }
    
    void    Menu::addBackground()
    {
        auto size = cocos2d::Director::getInstance()->getWinSizeInPixels();
        auto background = cocos2d::Sprite::create("./res/home.jpg");
        background->setScale(size.width / background->getSpriteFrame()->getRect().size.width,
                             size.height / background->getSpriteFrame()->getRect().size.height);
        addChild(background, -1);
    }
    
    void    Menu::addMenu()
    {
        auto campaign = cocos2d::MenuItemFont::create("Campaign",
                                                      CC_CALLBACK_1(Menu::campaign, this));
        auto standard = cocos2d::MenuItemFont::create("Standard Game",
                                                      CC_CALLBACK_1(Menu::standard, this));
        auto multiplayer = cocos2d::MenuItemFont::create("Multiplayer",
                                                         CC_CALLBACK_1(Menu::multiplayer, this));
        auto load = cocos2d::MenuItemFont::create("Loading",
                                                  CC_CALLBACK_1(Menu::load, this));
        auto exit = cocos2d::MenuItemFont::create("Exit",
                                                  CC_CALLBACK_1(Menu::exit, this));
        
        campaign->setPosition(multiplayer->getPosition().x, multiplayer->getPosition().y + 100);
        standard->setPosition(multiplayer->getPosition().x, multiplayer->getPosition().y + 50);
        load->setPosition(multiplayer->getPosition().x, multiplayer->getPosition().y - 50);
        exit->setPosition(multiplayer->getPosition().x, multiplayer->getPosition().y - 100);
        
        auto menu = cocos2d::Menu::create(campaign, standard, multiplayer, load, exit, NULL);
        addChild(menu, 1);
    }
    
    // Menu functions

    void    Menu::campaign(cocos2d::Ref *sender)
    {
        cocos2d::Director::getInstance()->pushScene(Campaign::createScene());
    }
    
    void    Menu::standard(cocos2d::Ref *sender)
    {
        cocos2d::Director::getInstance()->pushScene(StandardGame::createScene());
    }

    void    Menu::multiplayer(cocos2d::Ref *sender)
    {
        cocos2d::Director::getInstance()->pushScene(Multiplayer::createScene());
    }
    
    void    Menu::load(cocos2d::Ref *sender)
    {
        cocos2d::Director::getInstance()->pushScene(Load::createScene());
    }

    void    Menu::exit(cocos2d::Ref *sender)
    {
        cocos2d::Director::getInstance()->end();
    }

}