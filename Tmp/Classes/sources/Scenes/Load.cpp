//
//  Load.cpp
//  Tmp
//
//  Created by siffer_g on 29/07/2015.
//
//

#include "Scenes/Load.h"

namespace Scenes
{
    
    cocos2d::Scene  *Load::createScene()
    {
        auto scene  = cocos2d::Scene::create();
        auto layer  = Load::create();
        
        scene->addChild(layer);
        return (scene);
    }
    
    bool    Load::init()
    {
        if (!cocos2d::Layer::init())
            return (false);
        return (true);
    }
}