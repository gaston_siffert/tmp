set(PLATFORM_SPECIFIC_SRC)
set(PLATFORM_SPECIFIC_HEADERS)
if(MACOSX OR APPLE)
    set(PLATFORM_SPECIFIC_SRC
    #  proj.ios_mac/ios/main.m
    #  proj.ios_mac/ios/RootViewController.mm
    #  proj.ios_mac/ios/AppController.mm
            proj.ios_mac/mac/main.cpp
    )
    set(PLATFORM_SPECIFIC_HEADERS
    #  proj.ios_mac/ios/RootViewController.h
    #  proj.ios_mac/ios/AppController.h
    )
elseif(LINUX) #assume linux
    set(PLATFORM_SPECIFIC_SRC
            proj.linux/main.cpp
    )
elseif ( WIN32 )
    set(PLATFORM_SPECIFIC_SRC
            proj.win32/main.cpp
    )
    set(PLATFORM_SPECIFIC_HEADERS
            proj.win32/main.h
            proj.win32/resource.h
    )
endif()

include_directories(
        /usr/local/include/GLFW
        /usr/include/GLFW
        ${COCOS2D_ROOT}/cocos
        ${COCOS2D_ROOT}/cocos/platform
        ${COCOS2D_ROOT}/cocos/audio/include/
        Classes
)
if ( WIN32 )
    include_directories(
            ${COCOS2D_ROOT}/external/glfw3/include/win32
            ${COCOS2D_ROOT}/external/win32-specific/gles/include/OGLES
    )
endif( WIN32 )



set(SourceDir Classes/sources)
set(IncludeDir Classes/includes)

file(GLOB_RECURSE Sources "${PROJECT_SOURCE_DIR}/${SourceDir}/*.cpp")
file(GLOB_RECURSE Headers "${PROJECT_SOURCE_DIR}/${IncludeDir}/*.h")

set(GAME_SRC
        ${Sources}
        ${PLATFORM_SPECIFIC_SRC})

set(GAME_HEADERS
        ${Headers}
        ${PLATFORM_SPECIFIC_HEADERS})

include_directories(${PROJECT_SOURCE_DIR}/${IncludeDir})